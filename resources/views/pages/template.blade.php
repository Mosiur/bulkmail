@extends('master')
@section('content')
<!-- edit replay message modal -->
<form method="POST" action="{{url('/updt_template')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="edittemplate">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update template</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body" id="templatedetails">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">All Template</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Pages</a></li>
				<li class="breadcrumb-item active" aria-current="page">All Template</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							All Template
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Serial</th>
										<th>Campaugn Group</th>
										<th>Subject</th>
										<th>Message</th>
										<th>Atttachment</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<?php $counter=0;?>
									@foreach($templates as $row)
									<tr>
										<td><?php echo ++$counter; ?></td>

										<?php
										foreach ($camgroup as $key => $group) {
											if ($group->id == $row->campaigngroup) {
												echo "<td>".$group->name."</td>";
											}
										}
										?>
										<td>{{$row->subject}}</td>
										<td>{!!$row->message!!}</td>
										<td>{{$row->attachment}}</td>
										

										<td>
											<a class="text-info" style="font-size: 22px;" onclick="edit_template('{{$row->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
											</a>
										</td>
										<td>
											<a class="text-warning" style="font-size: 22px;"  href="{{ route('delete_template', ['id' => $row->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i></a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->

	</div>
</div>
<!--Main Content-->
@endsection