@extends('master')
@section('content')
<form method="POST" action="{{url('/uploadcsv')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="fileupload">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Upload Excel file(Suppoeted type .xls,.xlsx)</h6>
				</div>
				<div class="modal-body" id="linkform">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">File:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" type="file" name="file" id="file">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Upload</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>




<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Mail Library</h3>
			<span class="success" style="color:green; margin-top:10px; margin-bottom: 10px;"></span>
			<span class="failed" style="color:red; margin-top:10px; margin-bottom: 10px;"></span>
			<?php
			if (session('success')){?>
				<div class="btn-success" style="color:white; text-align: center; font-size: 20px;">{{ session('success') }}</div>
				<?php
			}
			if(session('errors')){
				if(is_array(session('errors'))){
					foreach($errors as $error)?>
						<div class="btn-danger" style="color:white; text-align: center; font-size: 20px;">{{$error}}</div>
					<?php 
				}else{
					?>
					<div class="btn-warning" style="color:white; text-align: center; font-size: 20px;">{{ session('errors') }}</div>
					<?php
				}
			}
			?>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Mail library</li>
			</ol>
		</div>
	</div><!--Main Content Container-->

	<!--Main Content Container-->
	<div class="container-fluid pd-t-0">
		<!--Page Header-->
		<div class="page-header" style="margin: 0px !important;">
			<div class="col-md-2">
				<button style="border-radius: 10px; float: left; width: 100%;" id="active1" class=" btn btn-info">Mail Library</button>
			</div>
			<div class="col-md-2">
				<button style="border-radius: 10px; float: left;width: 100%;" id="inactive"class=" btn btn-warning">Inactive Library</button>
			</div>
			<div class="col-md-8"></div>	
		</div>
	</div><!--Main Content Container-->

	<!--Table start-->
	<div class="container-fluid pd-t-20" id="active1_table">
		<!--Row-->
		<div class="card mg-b-20">
			<div class="card-body">
				<div class="table-responsive">
					<button id="button" class="btn btn-primary btn-sm mg-b-20" data-toggle="modal" data-target="#fileupload">Upload Excel</button>
					<a href="{{url('assets\uploads\file\sample\sample_smtp.xlsx')}}"><button id="button" class="btn btn-primary btn-sm mg-b-20">Download Sample file</button></a>
					<table class="table text-md-nowrap" id="example1">
						<thead>
							<tr> 
								<th>SL</th>
								<th>Campaign</th>
								<th>Group</th>
								<th>Email</th>
								<th>Send Status</th>
								<th>Validity</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php $counter=0;?>
							@foreach($activemaillibrary as $row)
							<tr>
								<td><?php echo ++$counter; ?></td>
								<?php 
								foreach ($campaigns as $key => $value) {
									if($value->id == $row->campaignid){ 
										echo "<td>".$value->campaignname."</td>";
									}
								}

								foreach ($group as $key => $grpdata) {
									if($grpdata->id == $row->campaingroup){ 
										echo "<td>".$grpdata->name."</td>";
									}
								}
								?>
								<td>{{$row->emailaddress}}</td>
								<?php if($row->flag==0){ $sendstatus="Not Send";} if($row->flag==1){ $sendstatus="Send";}?>
								<td><?php echo $sendstatus;?></td>
								<td><?php if($row->varifiedmail == '1'){ echo "Valid";}else{echo "Invalid";}?></td>
								<!-- <td onblur="update('{{$row->id}}')" id="{{$row->id}}"contenteditable><?php echo ($row->status == '1')?'Active':'Inactive'; ?></td> -->
								<?php
								if($row->status == '1'){
									?>
									<td>
										<button onclick="update('{{$row->id}}','{{$row->status}}')" id="{{$row->id}}" class="btn btn-success" style="border-radius: 10px;">
											<?php echo "Active";?>
										</button>
									</td>
									<?php
								}else{?>
									<td>
										<button onclick="update('{{$row->id}}','{{$row->status}}')" id="{{$row->id}}" class="btn btn-warning" style="border-radius: 10px;">
											<?php echo "Active";?>
										</button>
									</td>
									<?php
								}
								?>

							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div>
	<!--table end-->

	<!--Table start-->
	<div class="container-fluid pd-t-20" id="inactive_table">
		<!--Row-->
		<div class="card mg-b-20">
			<div class="card-body">
				<div class="table-responsive">
					<!-- <button id="button" class="btn btn-primary btn-sm mg-b-20">Delete Multiple</button> -->
					<table class="table text-md-nowrap" id="example2" style="width: 100% !important;">
						<thead>
							<tr> 
								<th>SL</th>
								<th>Campaign</th>
								<th>Group</th>
								<th>Email</th>
								<th>Validity</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $counter=0;?>
							@foreach($inactivedata as $data)
							<tr>
								<td><?php echo ++$counter; ?></td>
								<?php 
								foreach ($campaigns as $key => $value) {
									if($value->id == $row->campaignid){ 
										echo "<td>".$value->campaignname."</td>";
									}
								}

								foreach ($group as $key => $grpdata) {
									if($grpdata->id == $row->campaingroup){ 
										echo "<td>".$grpdata->name."</td>";
									}
								}
								?>
								<td>{{$data->emailaddress}}</td>
								<td><?php if($data->varifiedmail == '1'){ echo "Valid";}else{echo "Invalid";}?></td>
								<?php
								if($data->status == '1'){
									?>
									<td>
										<button onclick="update('{{$data->id}}','{{$data->status}}')" id="{{$data->id}}" class="btn btn-success" style="border-radius: 10px;">
											<?php echo "Active";?>
										</button>
									</td>
									<?php
								}else{?>
									<td>
										<button onclick="update('{{$data->id}}','{{$data->status}}')" id="{{$data->id}}" class="btn btn-danger" style="border-radius: 10px;">
											<?php echo "Inactive";?>
										</button>
									</td>
									<?php
								}
								?>
								<td onclick="return confirm('Are you sure you want to delete this item?');">
									<button  onclick="delete_smtp('{{$data->id}}')" class="btn btn-warning" style="border-radius: 10px;">Delete</button>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div>
	<!--table end-->


</div>
<!--Main Content-->
<script>
	$(document).ready(function(){
		$("#active1_table").show();
		$("#active2_table,#inactive_table").hide();

		$("#active1").click(function(){
			$("#active1_table").show();
			$("#active2_table,#inactive_table").hide();
		});
		$("#active2").click(function(){
			$("#active2_table").show();
			$("#active1_table,#inactive_table").hide();
		});
		$("#inactive").click(function(){
			$("#inactive_table").show();
			$("#active1_table,#active2_table").hide();
		});
		
	});
</script>
<script type="text/javascript">
	function deleteConfirm(){
		var result = confirm("Are you sure to delete SMTP?");
		if(result){
			return true;
		}else{
			return false;
		}
	}
	$('#select_all_act1').click(function(event) {
		if(this.checked) {
      	// Iterate each checkbox
      	$(':checkbox').each(function() {
      		this.checked = true;
      	});
      }
      else {
      	$(':checkbox').each(function() {
      		this.checked = false;
      	});
      }
  });
	$('#select_all_act2').click(function(event) {
		if(this.checked) {
      	// Iterate each checkbox
      	$(':checkbox').each(function() {
      		this.checked = true;
      	});
      }
      else {
      	$(':checkbox').each(function() {
      		this.checked = false;
      	});
      }
  });

	$('#select_all_inact').click(function(event) {
		if(this.checked) {
      	// Iterate each checkbox
      	$(':checkbox').each(function() {
      		this.checked = true;
      	});
      }
      else {
      	$(':checkbox').each(function() {
      		this.checked = false;
      	});
      }
  });
</script>
<script type="text/javascript">
	
	function update(id,flag){
		// var status=$("#"+id).html();
		var token = "{{ csrf_token() }}";
		$.ajax({
			url:'{{url('/updateact1')}}',
			method:"POST",
			data:{
				id:id,
				flag:flag,				
				"_token":token,
			},
			success:function(Response){
				// console.log(Response);
				// alert(Response.success);
				if(Response) {
					$('.success').text(Response.success);
					$('.failed').text(Response.failed);
				}
			}
		});

	}
	function delete_smtp(id){
		var token = "{{ csrf_token() }}";
		$.ajax({
			url:'{{url('/delete_smtp')}}',
			method:"POST",
			data:{
				id:id,				
				"_token":token,
			},
			success:function(Response){
				// console.log(Response);
				// alert(Response.success);
				if(Response) {
					$('.success').text(Response.success);
					$('.failed').text(Response.failed);
				}
			}
		});
	}
</script>
@endsection