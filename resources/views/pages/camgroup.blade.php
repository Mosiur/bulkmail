@extends('master')
@section('content')
<?php $protocal=null;?>
<!-- start add master mail modal -->
<form method="POST" action="{{url('/storegroup')}}">
	@csrf
	<div class="modal" id="addgroup">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add Campaign Group</h6>
				</div>
				<div class="modal-body" id="groupform">
				</div>
			</div>
		</div>
	</div>
</form>
<!-- End of add mastermail Modal-->




<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updt_camgroup')}}">
	@csrf
	<div class="modal" id="editgroup">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">

				<div class="modal-body tx-center pd-y-20 pd-x-20" id="groupdetails">
				</div>

			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Campaign Group</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Campaign Group</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Campaign Group
							<div>
								<button class="btn btn-info" style="float: right; border-radius: 10px;" onclick="addgroup()">Add Group</button>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Campaign</th>
										<th>Group</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									<?php $count=0;?>
									@foreach($campaigns as $campaignname)
										@foreach($camgroup as $camgroup)
											@if($campaignname->id == $camgroup->campaignid)
												<tr>
													<td><?php echo ++$count;?></td>
													<td>{{$campaignname->campaignname}}</td>
													<td>{{$camgroup->name}}</td>

													<td>
														<a class="text-warning" style="font-size: 22px;" onclick="edit_camgroup('{{$camgroup->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
														</a>
													</td>
													<td>
														<a class=" text-danger" style="font-size: 22px;" href="{{ route('delete_camgroup', ['id' => $camgroup->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i>
														</a>
													</td>
												</tr>
											@endif
										@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection