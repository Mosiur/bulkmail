@extends('master')
@section('content')
<!-- edit replay message modal -->
<form method="POST" action="{{url('/adduser')}}">
	@csrf
	<div class="modal" id="adduser">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add User</h6>
				</div>
				<div class="modal-body">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Full Name</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" placeholder="Enter your fullname"  type="text" name="name" required="required">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Email</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" placeholder="Enter user email" type="email" name="email" required="required">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">User Name</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" placeholder="Enter your username" type="text" name="username" required="required">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Password</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input class="form-control" placeholder="Enter your password" type="password" name="password" required="required">
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Privilege</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<select class="form-control" name="privillage" required="required">
									<option value="1">User</option>
									<option value="2">Sub-Admin</option>
									<option value="1000">Admin</option>
								</select>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Campaign</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<select class="form-control" name="campaignid" required="required">
									@foreach($userdata as $value)
									<option value="{{$value->campaignid}}">{{$value->campaignname}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Status</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<select class="form-control" name="flag" required="required">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add User</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</form>


<form method="POST" action="{{url('/updateuser')}}">
	@csrf
	<div class="modal" id="updtuser">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Update User</h6>
				</div>
				<div class="modal-body" id="userdetails">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">User list</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
				<li class="breadcrumb-item active" aria-current="page">User list</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							User list
							<a style="float: right !important;" data-toggle="modal" data-target="#adduser"><button class="btn btn-info">Add User</button></a>
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Limit</th>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Password</th>
										<th>Status</th>
										<th>Privilige</th>
										<th>Join date</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($userdata as $row)
									<tr>
										<td>{{$row->campaignname}}</td>
										<td>{{$row->campaignlimit}}</td>
										<td>{{$row->name}}</td>
										<td>{{$row->email}}</td>
										<td>{{$row->username}}</td>
										<td>{{$row->password}}</td>

										<td>
											<?php
											if($row->flag == 1){
												echo '<span style="color:green">Active</span>';
											}else{
												echo '<span style="color:red">Inactive</span>';
											}?>
										</td>
										<td>
											<?php if($row->privillage == 1){
												echo "User";
											}elseif($row->privillage == 2){
												echo "Sub Admin";
											}elseif($row->privillage == 1000){
												echo "Admin";
											}else{
												echo "None";
											}?>
										</td>
										<td>
											<?php echo $row->created_at;?>
										</td>
										<td>
											<a class="text-info" style="font-size: 22px;" onclick="edit_user('{{$row->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->

	</div>
</div>
<!--Main Content-->
@endsection