@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Dashboard</h3>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Statics:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Today Send</th>
										<th>Not Send</th>
										<th>Total Send</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="cursor: progress;">1</td>
										<td>{{$tadaysend}}</td>
										<td>{{$notsend}}</td>
										<td>{{$totalsend}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Mail Library:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Valid Mail</th>
										<th>Invalid Mail</th>
										<th>Total Uploaded</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="cursor: progress;">1</td>
										<td style="cursor: progress;">{{$valid}}</td>
										<td style="cursor: progress;">{{$invalid}}</td>
										<td style="cursor: progress;">{{$totalmail}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60" style="padding-top: 26px;">
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Campaign Statics:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign Name</th>
										<th>Expaire At</th>
										<th>Existing Limit</th>
										<th>Total Limit</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									@foreach($campaign as $key =>$value )
									<tr>
										<td style="cursor: progress;">{{$value->campaignname}}</td>
										<td>{{$value->campaigndescription}}</td>
										<td>{{$value->campaignlimit}}</td>
										<td>{{$value->totallimit}}</td>
										<?php 
										if($value->flag==0){$flag="Inactive";$class="text-warning";}
										if($value->flag==1){$flag="Active";$class="text-success";}
										?>
										<td class="<?php echo $class;?>">{{$flag}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Master Mail:
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Address</th>
										<th>Assign Group</th>
										<th>Limit</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$count=0;
									?>
									@foreach($mastermails as $key => $mastermail)
									<tr>
										<td style="cursor: progress;"><?php echo ++$count;?></td>
										<td style="cursor: progress;">{{$mastermail->username}}</td>
										<td style="cursor: progress;">
											<?php
											foreach ($campaigngroup as $key => $row) {
												if($mastermail->group == $row->id){
													echo $row->name;
												}
											}
											?>
										</td>
										<td style="cursor: progress;">{{$mastermail->maxlimit}}</td>
										
										<?php
										if($mastermail->flag==1){
											$status="Active";
											$class="text-success";
										}
										if($mastermail->flag==0){
											$status="Inactive";
											$class="text-warning";
										}
										?>
										<td class="<?php echo $class;?>" style="cursor: progress;">{{$status}}</td>
										
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection