@extends('master')
@section('content')
<!-- start add master mail modal -->
<form method="POST" action="{{url('/storecorn')}}">
	@csrf
	<div class="modal" id="addcorn">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add Corn</h6>
				</div>
				<div class="modal-body" id="cornform">
				</div>
			</div>
		</div>
	</div>
</form>
<!-- End of add mastermail Modal-->




<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updt_corn')}}">
	@csrf
	<div class="modal" id="editcorn">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">

				<div class="modal-body tx-center pd-y-20 pd-x-20" id="corndetails">
				</div>

			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Campaign Corn</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Campaign Corn</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Campaign Corn
							<div>
								<button class="btn btn-info" style="float: right; border-radius: 10px;" onclick="addcorn()">Add Corn</button>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Campaign</th>
										<th>Hit(perminute)</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody>
									<?php $count=0;?>
									@foreach($campaigns as $campaignname)
										@foreach($corndata as $value)
											@if($campaignname->id == $value->campaignid)
												<tr>
													<td><?php echo ++$count;?></td>
													<td>{{$campaignname->campaignname}}</td>
													<td>{{$value->hits}}</td>

													<td>
														<a class="text-warning" style="font-size: 22px;" onclick="edit_corn('{{$value->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
														</a>
													</td>
												</tr>
											@endif
										@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection