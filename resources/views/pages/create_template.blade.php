@extends('master')
@section('content')
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Create emplate</h3>
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Pages</a></li>
				<li class="breadcrumb-item active" aria-current="page">Create Template</li>
			</ol>
		</div>
		<!--Page Header-->

		<div class="row">
			<div class="col-lg-12">
				<div class="main-content-body d-flex flex-column">

					<div class="card p-4">
						<!-- Row -->
						<div class="card-body">
							<div class="main-content-label mg-b-5">
								Create Template
							</div>

							<div class="pd-30 pd-sm-40 bg-light">
								<form action="{{url('/inserttemplate')}}" method="post" enctype="multipart/form-data">
									@csrf

									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Campaign:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<select class="form-control select2-no-search" name="campaignid" required="required">
												@foreach($campaign as $camvalue)
												<option value="<?php echo $camvalue->id;?>">{{$camvalue->campaignname}}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Campaign Group:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<select class="form-control select2-no-search" name="campaigngroup" required="required">
												@foreach($campaigngroup as $value)
												<option value="<?php echo $value->id;?>">{{$value->name}}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Subject:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<input name="subject" class="form-control" placeholder="Enter subject of mail" type="text" required="required">
										</div>
									</div>

									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Messsage:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<textarea required type="text" class="ckeditor" name="messagebody" placeholder="Enter replay message" ></textarea>
										</div>
									</div>


									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0">Attachment:</label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<input name="attachment" class="form-control" placeholder="Enter delay time for send message" type="file" required="required">
										</div>
									</div>

									<div class="row row-xs align-items-center mg-b-20">
										<div class="col-md-3">
											<label class="form-label mg-b-0"></label>
										</div>
										<div class="col-md-9 mg-t-5 mg-md-t-0">
											<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add Message</button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!--/Row-->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Main Content-->
@endsection