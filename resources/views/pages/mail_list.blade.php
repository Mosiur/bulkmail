@extends('master')
@section('content')
<!-- Add lead modal -->
<form method="POST" action="{{url('/insertmail')}}">
	@csrf
	<div class="modal" id="addmail">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add Mail</h6>
				</div>
				<div class="modal-body" id="leadform">
					<div class="pd-30 pd-sm-40 bg-light">
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4">
								<label class="form-label mg-b-0">Mail Address:</label>
							</div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<input  class="form-control" type="email" name="email" required>
							</div>
						</div>
						<div class="row row-xs align-items-center mg-b-20">
							<div class="col-md-4"></div>
							<div class="col-md-8 mg-t-5 mg-md-t-0">
								<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add Mail</button>
								<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>

<form method="POST" action="{{url('/update_mail')}}">
	@csrf
	<div class="modal" id="edit_mail">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Edit Mail</h6>
				</div>
				<div class="modal-body" id="mail_details">
				</div>
			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">
	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">
		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Mail list</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a></li>
				<li class="breadcrumb-item active" aria-current="page">Mail list</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Mail List
							<a class="btn btn-info" style="float: right !important; border-radius: 10px; margin: 5px;" data-target="#addmail" data-toggle="modal" >Add
							</a>
						</div>
						<div class="">
							<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>SL</th>
										<th>Email Address</th>
										<th>Created At</th>
										<th style="float: right !important;">Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($mail_list as $row)
									<tr>
										<td>{{$row->id}}</td>
										<td>{{$row->emailaddress}}</td>
										<td>{{$row->created_at}}</td>
										<td style="float: right !important;">
											<a class="btn btn-info" style="border-radius: 10px;" onclick="edit_mail('{{$row->id}}')">Edit
											</a>
											<a class="btn btn-warning" style="border-radius: 10px;"  href="{{ route('delete_mail', ['id' => $row->id])}}">Delete</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/Row -->

</div>
</div>

<!--Main Content-->
@endsection