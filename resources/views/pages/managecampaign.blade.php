@extends('master')
@section('content')
<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updt_campaign')}}">
	@csrf
	<div class="modal" id="editcampaign">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">

				<div class="modal-body tx-center pd-y-20 pd-x-20" id="campaigndetails">
				</div>

			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Manage Campaign</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Manage Campaign</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Campaign
						</div>
						<div class="table-responsive">
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign Name</th>
										<th>Existing Limit</th>
										<th>Total Limit</th>
										<th>Exp Date</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody>
									@foreach($campaigns as $campaignrow)
									<tr>
										<th scope="row">{{$campaignrow->campaignname}}</th>
										<td>{{$campaignrow->campaignlimit}}</td>
										<td>{{$campaignrow->totallimit}}</td>
										<td>{{$campaignrow->campaigndescription}}</td>
										<td>
											<a class="text-warning" style="font-weight: bold;font-size: 22px;" onclick="edit_campaign('{{$campaignrow->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
											</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection