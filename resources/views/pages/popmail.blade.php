@extends('master')
@section('content')
<?php $protocal=null;?>
<!-- start add master mail modal -->
<form method="POST" action="{{url('/insertpopmail')}}" enctype="multipart/form-data">
	@csrf
	<div class="modal" id="addpopmail">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content modal-content-demo">
				<div class="modal-header">
					<h6 class="modal-title">Add POP/Master mail</h6>
				</div>
				<div class="modal-body" id="popmailform">
				</div>
			</div>
		</div>
	</div>
</form>
<!-- End of add mastermail Modal-->




<!-- MODAL ALERT MESSAGE -->
<form method="POST" action="{{url('/updt_mastermail')}}">
	@csrf
	<div class="modal" id="editmastermail">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content tx-size-sm">

				<div class="modal-body tx-center pd-y-20 pd-x-20" id="mastermaildetails">
				</div>

			</div>
		</div>
	</div>
</form>
<!--Main Content-->
<div class="main-content px-0 app-content">

	<!--Main Content Container-->
	<div class="container-fluid pd-t-60">

		<!--Page Header-->
		<div class="page-header">
			<h3 class="page-title">Master Mail</h3>
			@if (session('success'))
			<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			@if (session('failed'))
			<div class="alert alert-danger">
				{{ session('failed') }}
			</div>
			@endif
			<ol class="breadcrumb mb-0">
				<li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page">Master Mail</li>
			</ol>
		</div>
		<!--Page Header-->
		<!--Row-->
		<div class="row row-sm">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="main-content-label mg-b-5">
							Master Mail
							<div>
								<button class="btn btn-info" style="float: right; border-radius: 10px;" onclick="addpopmail()">Add POP Mail</button>
							</div>
						</div>
						<div class="table-responsive">
							<style type="text/css">
								.table th, .table td {
									padding: 9px 7px;
									line-height: 1.462;
								}
							</style>
							<table class="table table-hover mb-0 text-md-nowrap">
								<thead>
									<tr>
										<th>Campaign</th>
										<th>Group</th>
										<th>Email ID</th>
										<th>Password</th>
										<th>Host Name</th>
										<th>Protocal</th>
										<th>Port</th>
										<th>Limit</th>
										<th>Status</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
									@foreach($campaigns as $campaignname)
										@foreach($mastermails as $mastermailsrow)
											@if($campaignname->id == $mastermailsrow->campaignid)
												<tr>
													<th scope="row">{{$campaignname->campaignname}}</th>
													<?php
													foreach ($camgroup as $key => $group) {
														if($mastermailsrow->group == $group->id){
															?>
															<td>{{$group->name}}</td>
															<?php

														}
													}
													?>												
													<td>{{$mastermailsrow->username}}</td>
													<td>{{$mastermailsrow->password}}</td>
													<td>{{$mastermailsrow->host}}</td>
													<?php 
													if($mastermailsrow->protocol == 1){
														$protocal='SSL';
													}else{
														$protocal='TLS';
													}
													?>
													<td>{{$protocal}}</td>
													<td>{{$mastermailsrow->port}}</td>
													<td>{{$mastermailsrow->maxlimit}}</td>
													@if($mastermailsrow->flag==0)
														<td>
															<a class="text-danger" style="font-weight: bold;">inactive</a>
														</td>
														
													@else
														<td>
															<a class="text-success" style="font-weight: bold;">active</a>
														</td>
													@endif
													<td>
														<a class="text-warning" style="font-size: 22px;" onclick="edit_mastermail('{{$mastermailsrow->id}}')"><i class="si si-note" data-toggle="tooltip" title="Edit"></i>
														</a>
													</td>
													<td>
														<a class=" text-danger" style="font-size: 22px;" href="{{ route('delete_mastermail', ['id' => $mastermailsrow->id])}}"><i class="ti-trash" data-toggle="tooltip" title data-original-title="delete"></i>
														</a>
													</td>
												</tr>
											@endif
										@endforeach
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/Row -->
	</div><!--Main Content Container-->
</div>
<!--Main Content-->
@endsection