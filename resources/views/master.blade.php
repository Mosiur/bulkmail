<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta name="Description" content="HTML5 Bootstrap Admin Template">
	<meta name="author" content="Spruko Technologies Private Limited">
	<meta name="keywords" content="dashboard template,admin template,bootstrap dashboard,crypto dashboard,cryptocurrency dashboard,cryptocurrency website template,bitcoin template,bootstrap admin template,ico website template,admin dashboard template,admin panel template,bootstrap 4 admin template,dashboard html,html admin template,simple bootstrap template,template admin bootstrap 4,admin dashboard template bootstrap">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<script type="text/javascript">
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	</script>

	<!-- Favicon -->
	<link rel="icon" href="{{asset('assets/img/brand/favicon.ico')}}" type="image/x-icon"/>

	<!-- Title -->
	<title>Nixlot - HTML5 Bootstrap Admin Template</title>

	<!-- Font Awesome -->
	<link href="{{asset('assets/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet">

	<!-- Bootstrap -->
	<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

	<!-- Ionicons -->
	<link href="{{asset('assets/plugins/ionicons/css/ionicons.min.css')}}" rel="stylesheet">

	<!-- Typicons -->
	<link href="{{asset('assets/plugins/typicons.font/typicons.css')}}" rel="stylesheet">

	<!-- Sidebar css -->
	<link href="{{asset('assets/plugins/sidebar/sidebar.css')}}" rel="stylesheet">

	<!-- Side menu css-->
	<link href="{{asset('assets/plugins/sidemenu/closed/sidemenu.css')}}" rel="stylesheet">

	<!-- lightslider Css -->
	<link href="{{asset('assets/plugins/lightslider/css/lightslider.min.css')}}" rel="stylesheet">

	<!-- Custom Scroll bar-->
	<link href="{{asset('assets/plugins/mscrollbar/jquery.mCustomScrollbar.css')}}" rel="stylesheet"/>

	<!-- Select2 css -->
	<link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet">

	<!-- morris css -->
	<link href="{{asset('assets/plugins/morris.js/morris.css')}}" rel="stylesheet">

	<!--Bootstrap-daterangepicker css-->
	<link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet" >

	<!-- Default Style -->
	<link href="{{asset('assets/css/dashboard-four.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/dashboard-four-dark.css')}}" rel="stylesheet">
	<link href="{{asset('assets/css/dashboard-four-dark.css')}}" rel="stylesheet">

	<!-- Icon Style -->
	<link href="{{asset('assets/css/icons.css')}}" rel="stylesheet">

	<!--crypto Css -->
	<link href="{{asset('assets/plugins/cryptofont/css/cryptofont.min.css')}}" rel="stylesheet">

	<!--Flag Css -->
	<link href="{{asset('assets/plugins/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
	<!--Custom  fonrawsome -->
	<script src="https://kit.fontawesome.com/644c1f6431.js" crossorigin="anonymous"></script>
	<!-- Data table css -->
	<link href="{{asset('assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
	<link rel="stylesheet" href="{{asset('assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}">
	<link href="{{asset('assets/plugins/datatable/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<!-- Ckeditor -->
	<script src="{{asset('assets/ckeditor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			setInterval(function() {
				$('#time').load('{{url('/autoload')}}');
			}, 1000);
			$.ajaxSetup({ cache: false });
		});
	</script>
</head>

<body class="main-body app sidebar-mini">
	<!-- Loader -->
	<div id="loading">
		<img src="{{asset('assets/img/loader4.svg')}}" class="loader-img" alt="Loader">
	</div>

	<!-- main-header -->
	@include('layouts.header')
	<!--/main-header-->

	<!--App Sidebar-->
	@include('layouts.sidebar')
	<!--/App Sidebar-->

	<!--Main Content-->
	@yield('content')
	<!--Main Content-->
	<!--footer-->
	@include('layouts.footer')
	<!--/footer-->
	<!--Sidebar-right-->
	
	<!--/Sidebar-right-->

	<!-- JQuery min js -->
	<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>

	<!-- Datepicker js -->
	<script src="{{asset('assets/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>

	<!-- Bootstrap Bundle js -->
	<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

	<!-- Eva-Icons js -->
	<script src="{{asset('assets/plugins/web-fonts/eva.min.js')}}"></script>

	<!-- Ionicons js -->
	<script src="{{asset('assets/plugins/ionicons/ionicons.js')}}"></script>

	<!--Chart bundle min js -->
	<script src="{{asset('assets/plugins/chart.js/Chart.bundle.min.js')}}"></script>

	<!-- Flot js -->
	<script src="{{asset('assets/plugins/jquery.flot/jquery.flot.js')}}"></script>
	<script src="{{asset('assets/plugins/jquery.flot/jquery.flot.pie.js')}}"></script>
	<script src="{{asset('assets/plugins/jquery.flot/jquery.flot.resize.js')}}"></script>

	<!--Peity Chart js -->
	<script src="{{asset('assets/plugins/peity/jquery.peity.min.js')}}"></script>

	<!-- JQuery sparkline js -->
	<script src="{{asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

	<!-- Sampledata js -->
	<script src="{{asset('assets/js/chart.flot.sampledata.js')}}"></script>

	<!-- Moment js -->
	<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>

	<!-- select2.min js -->
	<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
	<script src="{{asset('assets/js/select2.js')}}"></script>

	<!--Bootstrap-daterangepicker js-->
	<script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

	<!-- Sidebar js -->
	<script src="{{asset('assets/plugins/sidebar/sidebar.js')}}"></script>
	<script src="{{asset('assets/plugins/sidebar/sidebar-custom.js')}}"></script>

	<!-- Custom Scroll bar Js-->
	<script src="{{asset('assets/plugins/mscrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

	<!-- Vector map js -->
	<script src="{{asset('assets/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
	<script src="{{asset('assets/plugins/jqvmap/maps/jquery.vmap.world.js')}}"></script>

	<!-- Vector map Sampledata js -->
	<script src="{{asset('assets/js/jquery.vmap.sampledata.js')}}"></script>

	<!-- Side-menu JS-->
	<script src="{{asset('assets/plugins/sidemenu/closed/sidemenu.js')}}"></script>

	<!-- Perfect-scrollbar js -->
	<script src="{{asset('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

	<!-- Index js -->
	<script src="{{asset('assets/js/index4.js')}}"></script>

	<!-- Lightslider js -->
	<script src="{{asset('assets/plugins/lightslider/js/lightslider.min.js')}}"></script>

	<!-- Custom js -->
	<script src="{{asset('assets/js/custom.js')}}"></script>
	<script src="{{asset('assets/js/chart.flot.sampledata.js')}}"></script>
	<script src="{{asset('assets/js/dashboard.sampledata.js')}}"></script>

	<!-- Morris js -->
	<script src="{{asset('assets/plugins/raphael/raphael.min.js')}}"></script>
	<script src="{{asset('assets/plugins/morris.js/morris.min.js')}}"></script>


	<!-- Data tables -->
	<script src="{{asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/pdfmake.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/vfs_fonts.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
	<!-- Datatable js -->
	<script src="{{asset('assets/js/table-data.js')}}"></script>

	<!-- Cusrom JS -->
	<!-- <script type="text/javascript" src="{{asset('assets/js/action.js')}}"></script> -->


<!-- 	<script>
		$(document).ready(function(){
			setInterval(function(){
				var dt= new Date();
				$("#time").text(dt.getHours()+":"+dt.getMinutes()+":"+dt.getSeconds());
			}, 1000);
			$.ajaxSetup({ cache: false });
		});
	</script> -->
	<script type="text/javascript">
		function edit_campaign(id){
			$.ajax({
				url:'{{url('/edit_campaign')}}',
				method:"GET",
				data:{id:id},
				success:function(data){
					$('#campaigndetails').html(data);
					$('#editcampaign').modal('show');
				}
			});
		}

		function edit_mastermail(id){
			$.ajax({
				url:'{{url('/edit_mastermail')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					
					$('#mastermaildetails').html(data);
					$('#editmastermail').modal('show');
				}
			});
		}
		function edit_replaymessage(id){
			$.ajax({
				url:'{{url('/edit_by_modal')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					
					$('#msgdetails').html(data);
					$('#editmsg').modal('show');
				}
			});

		}
		function edit_blacklist(id){
			$.ajax({
				url:'{{url('/edit_blacklist')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					
					$('#blacklistdetails').html(data);
					$('#upblacklist').modal('show');
				}
			});
		}
		function edit_user(id){
			$.ajax({
				url:'{{url('/edit_user')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					
					$('#userdetails').html(data);
					$('#updtuser').modal('show');
				}
			});
		}
		function edit_mail(id){
			$.ajax({
				url:'{{url('/edit_mail')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					
					$('#mail_details').html(data);
					$('#edit_mail').modal('show');
				}
			});
		}
		function edit_link(id){

			$.ajax({
				url:'{{url('/edit_link')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					
					$('#link_details').html(data);
					$('#edit_link').modal('show');
				}
			});
		}
		function addpopmail(){
			$.ajax({
				url:'{{url('/addpopmail')}}',
				method:"GET",
				success:function(data){
					
					$('#popmailform').html(data);
					$('#addpopmail').modal('show');
				}
			});
		}

		function edit_template(id){
			
			$.ajax({
				url:'{{url('/edit_template')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					$('#templatedetails').html(data);
					$('#edittemplate').modal('show');
				}
			});
		}

		function addgroup(){
			$.ajax({
				url:'{{url('/add_camgroup')}}',
				method:"GET",
				success:function(data){
					$('#groupform').html(data);
					$('#addgroup').modal('show');
				}
			});
		}

		function edit_camgroup(id){
			
			$.ajax({
				url:'{{url('/edit_camgroup')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					$('#groupdetails').html(data);
					$('#editgroup').modal('show');
				}
			});
		}
		function addcorn(){
			$.ajax({
				url:'{{url('/add_corn')}}',
				method:"GET",
				success:function(data){
					$('#cornform').html(data);
					$('#addcorn').modal('show');
				}
			});
		}
		function edit_corn(id){
			$.ajax({
				url:'{{url('/edit_corn')}}',
				method:"GET",
				data:{
					id:id,
				},
				success:function(data){
					$('#corndetails').html(data);
					$('#editcorn').modal('show');
				}
			});
		}
	</script>
</body>
</html>