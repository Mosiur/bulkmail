<!-- App Sidebar -->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
	<div class="col-lg-12" style="margin-top: 12px !important;">
		<div class="main-profile-overview">
			<div class="main-img-user profile-user">
				<img alt="" src="{{asset('assets/img/users/male/avatar.png')}}">
				<a href="JavaScript:void(0);" class="fas fa-camera profile-edit"></a>
			</div>
			<div id="time"></div>
		</div><!-- main-profile-overview -->
	</div>
	<ul class="side-menu">
		<li class="slide">
			<a class="side-menu__item" href="{{url('/')}}"><i class="side-menu__icon fas fa-tv"></i><span class="side-menu__label">Dashboard</span></a>
		</li>
		<li class="slide">
			<a class="side-menu__item" href="{{url('/managecampaign')}}"><i class="side-menu__icon fas fa-cog" ></i><span class="side-menu__label">Manage Campaign</span></a>
		</li>
		<li class="slide">
			<a class="side-menu__item" href="{{url('/camgroup')}}"><i class="side-menu__icon fas fa-users"></i><span class="side-menu__label">Campaign Group</span></a>
		</li>
		<li class="slide">
			<a class="side-menu__item" href="{{url('/popmail')}}"><i class="side-menu__icon fas fa-envelope-open-text"></i><span class="side-menu__label">Master Mail</span></a>
		</li>
		
		<li class="slide">
			<a class="side-menu__item" data-toggle="slide" href="#"><i class="side-menu__icon fas fa-file-alt"></i><span class="side-menu__label"> Mail Template</span><i class="angle fe fe-chevron-down"></i></a>
			<ul class="slide-menu">
				<li><a class="slide-item" href="{{url('/create_template')}}">Create Template</a></li>
				<li><a class="slide-item" href="{{url('/all_template')}}">All Template</a></li>
			</ul>
		</li>
		<?php
		if(Session::get('user')['privillage']==1000){?>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/corn')}}"><i class="side-menu__icon fas fa-link"></i><span class="side-menu__label">Corn</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/mail_library')}}"><i class="side-menu__icon fas fa-link"></i><span class="side-menu__label">Mail library</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/manageuser')}}"><i class="side-menu__icon fas fa-users"></i><span class="side-menu__label">Manage User</span></a>
			</li>
			<li class="slide">
				<a class="side-menu__item" href="{{url('/reset_system')}}"><i class="side-menu__icon fas fa-redo"></i><span class="side-menu__label">Reset System</span></a>
			</li>			
			<?php
		}
		?>
	</ul>
</aside>
<!--/App Sidebar