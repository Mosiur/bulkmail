<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class userController extends Controller
{
	public function alluser(){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=DB::table('user')
		->join('campaign','campaign.id','=','user.campaignid')
		->select('user.*','campaign.campaignname' ,'campaign.campaignlimit')
		->where('user.campaignid','=',$campaignid)
		->orderBy('user.id','DESC')
		->get();
		return view('pages.userlist',['userdata'=>$data]);

	}
	public function insert(Request $request){

		$name=$request->input('name');
		$email=$request->input('email');
		$username=$request->input('username');
		$password=md5($request->input('password'));
		$privillage=$request->input('privillage');
		$campaignid=$request->input('campaignid');
		$flag=$request->input('flag');

		$count=DB::table('user')->where('email','=',$email)->count();
		if($count < 1){
			$affected=DB::table('user')->insert(['name'=>$name,'email'=>$email,'username'=>$username,'password'=>$password,'privillage'=>$privillage,'campaignid'=>$campaignid,'flag'=>$flag]);
			if($affected){
				return redirect('/manageuser')->with('success', 'Record Added successfully!');
			}else{
				return redirect('/manageuser')->with('failed', 'Failed to Add Record!');
			}
		}else{
			return redirect('/manageuser')->with('failed', 'Username Already taken try another..!');
		}
	}
	public function edituser(){
		$id=$_GET['id'];
		$data=DB::table('user')->where('id','=',$id)->first();
		$campaigndata=DB::table('campaign')->select('id','campaignname')->get();
		?>
		<input type="hidden" name="id" value="<?php echo $id;?>">
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Full Name</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter your fullname"  type="text" name="name" value="<?php echo $data->name;?>" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Email</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter user email" type="email" name="email" value="<?php echo $data->email;?>" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">User Name</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter your username" type="text" name="username" value="<?php echo $data->username;?>" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Password</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter your password" type="password" name="password" value="<?php echo $data->password;?>" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Privilege</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="privillage" required>
					<option value="1" <?php if($data->privillage == 1){echo "selected";}?>>User</option>
					<option value="2" <?php if($data->privillage == 2){echo "selected";}?>>Sub-Admin</option>
					<option value="1000" <?php if($data->privillage == 1000){echo "selected";}?>>Admin</option>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Campaign</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="campaignid" required>
					<?php
					foreach($campaigndata as $row){?>
						<option value="<?php echo $row->id;?>">
							<?php echo $row->campaignname;?>
						</option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Status</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="flag" required>
					<option value="1" <?php if($data->flag == 1){echo "selected";}?>>Active</option>
					<option value="0" <?php if($data->flag == 0){echo "selected";}?>>Inactive</option>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4"></div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
				<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
			</div>
		</div>
		<?php
	}

	public function updateuser(Request $request){
		$name=$request->input('name');
		$email=$request->input('email');
		$username=$request->input('username');
		$password=md5($request->input('password'));
		$privillage=$request->input('privillage');
		$campaignid=$request->input('campaignid');
		$flag=$request->input('flag');
		$id=$request->input('id');

		$affected=DB::table('user')->where('id','=',$id)->update(['name'=>$name,'email'=>$email,'username'=>$username,'password'=>$password,'privillage'=>$privillage,'campaignid'=>$campaignid,'flag'=>$flag]);
		if($affected){
			return redirect('/manageuser')->with('success', 'Record Updated successfully!');
		}else{
			return redirect('/manageuser')->with('failed', 'Failed to Change!');
		}

	}
}
