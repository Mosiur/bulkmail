<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class auth extends Controller
{
    public function index(Request $request){
    	$email=$request->input('email');
    	$password=md5($request->input('password'));
    	$user=DB::table('user')->WHERE([['email','=',$email],['password','=',$password]])->first();
		
		if($user!=null){
			Session::put('user', ['name'=>$user->name,'email'=>$user->email,'campaignid'=>$user->campaignid,'privillage'=>$user->privillage]);
			$campaign = DB::table('campaign')->WHERE('id','=',$user->campaignid)->first();
			Session::put('campaign', ['campaignid'=>$campaign->id,'campaignname'=>$campaign->campaignname,'campaigndescription'=>$campaign->campaigndescription,'campaignlimit'=>$campaign->campaignlimit]);
			return redirect('/');
		}
		else{
			return redirect('/admin')->with('status', 'Invalid User name or Password...!');
		}

    }
}
