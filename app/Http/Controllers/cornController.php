<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class cornController extends Controller
{
	public function corn(){
		$campaignid=Session::get('campaign')['campaignid'];
		$campaigns = DB::table('campaign')->where('id','=',$campaignid)->get();
		$corndata=DB::table('corn')->where('campaignid','=',$campaignid)->get();
		return view('pages.corn',['corndata'=>$corndata,'campaigns'=>$campaigns]);
	}

	public function add(){
		$campaignid=Session::get('campaign')['campaignid'];
		$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
		?>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Number of mail send (perminute)</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="the nummber of mail you want to send" type="text" name="hits" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Campaign</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="campaignid" required>
					<?php
					foreach($campaign as $row){?>
						<option value="<?php echo $row->id;?>">
							<?php echo $row->campaignname;?>
						</option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4"></div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add Corn</button>
				<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
			</div>
		</div>
		<?php
	}

	public function store(Request $request){
		$data=$request->all();
		$check=DB::table('corn')->where([['campaignid','=',$data['campaignid']]])->count();
		if ($check == 0) {
			unset($data['_token']);
			$affected=DB::table('corn')->insert($data);
			if($affected){
				return redirect('/corn')->with('success', 'Record Added successfully!');
			}else{
				return redirect('/corn')->with('failed', 'Failed to Add!');
			}
		}else{
			return redirect('/corn')->with('failed', 'Corn Already Added try for another Campaign...!');
		}
		

	}
	public function edit(){
		$id=$_GET['id'];
		$campaignid=Session::get('campaign')['campaignid'];
		$corn=DB::table('corn')->where('id','=',$id)->first();
		$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
		?>
		<input type="hidden" name="id" value="<?php echo $id;?>">
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Mail Per Minute</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter the number of mail you want to send Per Minute" type="text" name="hits" value="<?php echo $corn->hits;?>" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Campaign</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="campaignid" required>
					<?php
					foreach($campaign as $row){?>
						<option <?php if($corn->campaignid == $row->id){ echo "selected";}?> value="<?php echo $row->id;?>">
							<?php echo $row->campaignname;?>
						</option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4"></div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
				<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
			</div>
		</div>
		<?php
	}

	public function update(Request $request){
		$data=$request->all();
		unset($data['_token']);
		$affected=DB::table('corn')->where('id','=',$data['id'])->update($data);
		if($affected){
			return redirect('/corn')->with('success', 'Record Updated successfully!');
		}else{
			return redirect('/corn')->with('failed', 'Failed to Update!');
		}
	}
}
