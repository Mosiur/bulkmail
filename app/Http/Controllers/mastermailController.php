<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class mastermailController extends Controller
{
	public function mail(){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=DB::table('mail')->where('campaignid','=',$campaignid)->get();
		return view('pages.mail_list',['mail_list'=>$data]);
	}
	public function insertmail(Request $request){
		$campaignid=Session::get('campaign')['campaignid'];
		$email=$request->input('email');
		$affected=DB::table('mail')->insert(['emailaddress'=>$email,'campaignid'=>$campaignid]);
		if($affected){
			return redirect('/mail')->with('success', 'Record Store successfully!');
		}else{
			return redirect('/mail')->with('failed', 'Failed to Store Rcord..!');
		}

	}
	public function edit_mail(){
		$id=$_GET['id'];
		$data=DB::table('mail')->where('id','=',$id)->get();
		foreach ($data as $key => $value) {
			?>
			<div class="row row-xs align-items-center mg-b-20">
				<input type="hidden" name="id" value="<?php echo $value->id?>">
				<div class="col-md-4">
					<label class="form-label mg-b-0">Mail Address:</label>
				</div>
				<div class="col-md-8 mg-t-5 mg-md-t-0">
					<input class="form-control" placeholder="Enter Mail Address" type="email" name="email" value="<?php echo $value->emailaddress;?>" required>
				</div>
			</div>
			<div class="row row-xs align-items-center mg-b-20">
				<div class="col-md-4"></div>
				<div class="col-md-8 mg-t-5 mg-md-t-0">
					<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
					<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
				</div>
			</div>
			<?php
		}
	}
	public function update_mail(Request $request){
		$id=$request->input('id');
		$emailaddress=$request->input('email');
		$affected=DB::table('mail')->Where('id','=',$id)->update(['emailaddress'=>$emailaddress]);
		if($affected){
			return redirect('/mail')->with('success', 'Record Updated successfully!');
		}else{
			return redirect('/mail')->with('failed', 'Failed to Update Rcord..!');
		}
	}
	public function delete_mail($id){
		$affected=DB::table('mail')->Where('id','=',$id)->delete();
		if($affected){
			return redirect('/mail')->with('success', 'Record Deleted successfully!');
		}else{
			return redirect('/mail')->with('failed', 'Failed to Delete Rcord..!');
		}

	}
}
