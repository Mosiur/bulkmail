<?php
namespace App\Http\Controllers;
use App\Classes\VerifyEmail;
use Illuminate\Http\Request;
use DB;
use Session;
use File;
use Image;
use Validator;
use Importer;

class maillibraryController extends Controller
{
	public function index(){
		$campaignid=Session::get('campaign')['campaignid'];
		$campaigns = DB::table('campaign')->where('id','=',$campaignid)->get();
		$group=DB::table('campaigngroup')->where('campaignid','=',$campaignid)->get();

		$activemaillibrary=DB::table('maillist')->where([['campaignid','=',$campaignid],['status','=',1]])->get();
		$inactivemaillibrary=DB::table('maillist')->where([['campaignid','=',$campaignid],['status','=',0]])->get();

		return view('pages.maillibrary',['activemaillibrary'=>$activemaillibrary,'inactivedata'=>$inactivemaillibrary,'campaigns'=>$campaigns,'group'=>$group]);
	}


	public function updateact1(Request $request){
		$id = $request->input('id');
		$flag = $request->input('flag');
		if($flag==1){
			$affected=DB::table('maillist')->where('id','=',$id)->update(['status'=>0]);
		}
		if($flag == 0){
			$affected=DB::table('maillist')->where('id','=',$id)->update(['status'=>1]);
		}
		
		if($affected){
			return response()->json(['success'=>'Record Updated successfully'],200);

		}else{
			return response()->json(['failed'=>'Faild to Updated Record'],200);
		}
	}

	public function delete_smtp(Request $request){

		$id = $request->input('id');
		if($id != NULL){
			$affected=DB::table('maillist')->where('id','=',$id)->delete();
		}
		
		if($affected){
			return response()->json(['success'=>'Record Deleted successfully'],200);

		}else{
			return response()->json(['failed'=>'Faild to Delete Record'],200);
		}
	}

    //Upload CSV file
	public function uploadcsv(Request $request){
		$campaignid=Session::get('campaign')['campaignid'];
		$mastermail=DB::table('mastermail')->where('campaignid','=',$campaignid)->first();
		$validator = Validator::make($request->all(),
			['file' => 'required|max:5000|mimes:xls,xlsx']
		);
		if($validator->passes()){
			$time=time();
			$file=$request->file('file');
			$filename=$time.$file->getClientOriginalName();
			$savepath=public_path('assets/uploads/file/');
			$file->move($savepath,$filename);


			$excel=Importer::make('Excel');
			$excel->load($savepath.$filename);
			$collection=$excel->getCollection();
			if(sizeof($collection[1])==4){
				$success=array();
				$failed=array();
				for ($i=1; $i < sizeof($collection); $i++) { 
					try{

						// Initialize library class
						$mail = new VerifyEmail();

						// Set the timeout value on stream
						$mail->setStreamTimeoutWait(20);

						// Set debug output mode
						$mail->Debug= TRUE; 
						$mail->Debugoutput= 'html'; 

						// Set email address for SMTP request
						$mail->setEmailFrom($mastermail->username);

						// Email to check
						$email = $collection[$i][2]; 

						// Check if email is valid and exist
						if($mail->check($email)){ 
							$import_data[]=array(
								"campaignid" =>$collection[$i][0],
								"campaingroup" =>$collection[$i][1],
								"emailaddress" =>$collection[$i][2],
								"varifiedmail" =>1,
								"status" =>$collection[$i][4],
								"flag" =>0
							);
							if(!empty($import_data)){
								$affected=DB::table('maillist')->insert($import_data);
								if ($affected) {
									$success[]='1';
									unset($import_data);

								}else{
									$failed[]='2';
								}
							}else{
								return redirect()->back()->with('errors','File have No Data...');
							}
						}elseif(verifyEmail::validate($email)){ 
							$import_data[]=array(
								"campaignid" =>$collection[$i][0],
								"campaingroup" =>$collection[$i][1],
								"emailaddress" =>$collection[$i][2],
								"varifiedmail" =>0,
								"status" =>0,
								"flag" =>1
							);
							if(!empty($import_data)){
								$affected=DB::table('maillist')->insert($import_data);
								DB::table('allmaillist')->insert($import_data);
								if ($affected) {
									$success[]='1';
									unset($import_data);

								}else{
									$failed[]='2';
								}
							}else{
								return redirect()->back()->with('errors','File have No Data...');
							} 
						}else{ 
							$import_data[]=array(
								"campaignid" =>$collection[$i][0],
								"campaingroup" =>$collection[$i][1],
								"emailaddress" =>$collection[$i][2],
								"varifiedmail" =>0,
								"status" =>0,
								"flag" =>1
							);
							if(!empty($import_data)){
								$affected=DB::table('maillist')->insert($import_data);
								if ($affected) {
									$success[]='1';
									unset($import_data);

								}else{
									$failed[]='2';
								}
							}else{
								return redirect()->back()->with('errors','File have No Data...');
							}
						}
						
					}catch(\Exception $e){
						return redirect()->back()
						->with(['errors'=>$e->getMessage()]);
					}
				}
				if($success!=NULL){
					return redirect()->back()->with('success','Data Uploaded Successfully');
				}
				if($failed!=NULL){

					return redirect()->back()->with('errors','Something is wrong..');
				}
			}else{
				return redirect()->back()->with(['errors'=>[0=>'Column mismatch.....!']]);
			}
		}else{
			return redirect()->back()
			->with(['errors'=>$validator->errors()->all()]);
		}
	}
}
