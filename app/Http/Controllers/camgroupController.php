<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class camgroupController extends Controller
{
    public function index(){
    	$campaignid=Session::get('campaign')['campaignid'];
    	$data=DB::table('campaigngroup')->where('campaignid','=',$campaignid)->get();
    	$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
    	return view('pages.camgroup',['camgroup'=>$data,'campaigns'=>$campaign]);
    }

    public function add(){
    	$campaignid=Session::get('campaign')['campaignid'];
    	$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
    	?>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Group Name</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter your username" type="text" name="name" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Campaign</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="campaignid" required>
					<?php
					foreach($campaign as $row){?>
						<option value="<?php echo $row->id;?>">
							<?php echo $row->campaignname;?>
						</option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4"></div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Add Group</button>
				<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
			</div>
		</div>
    	<?php
    }

    public function store(Request $request){

    	$data=$request->all();
    	unset($data['_token']);
    	$affected=DB::table('campaigngroup')->insert($data);
		if($affected){
			return redirect('/camgroup')->with('success', 'Record Added successfully!');
		}else{
			return redirect('/camgroup')->with('failed', 'Failed to Add!');
		}

    }
    public function edit(){
    	$id=$_GET['id'];
    	$campaignid=Session::get('campaign')['campaignid'];
    	$camgroup=DB::table('campaigngroup')->where('id','=',$id)->first();
    	$campaign = DB::table('campaign')->where('id','=',$campaignid)->get();
    	?>
    	<input type="hidden" name="id" value="<?php echo $id;?>">
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Group Name</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<input class="form-control" placeholder="Enter Campaign Group name" type="text" name="name" value="<?php echo $camgroup->name;?>" required>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4">
				<label class="form-label mg-b-0">Campaign</label>
			</div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<select class="form-control" name="campaignid" required>
					<?php
					foreach($campaign as $row){?>
						<option <?php if($camgroup->campaignid == $row->id){ echo "selected";}?> value="<?php echo $row->id;?>">
							<?php echo $row->campaignname;?>
						</option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
		<div class="row row-xs align-items-center mg-b-20">
			<div class="col-md-4"></div>
			<div class="col-md-8 mg-t-5 mg-md-t-0">
				<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
				<button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal" type="button">Cancel</button>
			</div>
		</div>
		<?php
    }

    public function update(Request $request){
    	$data=$request->all();
    	unset($data['_token']);
    	$affected=DB::table('campaigngroup')->where('id','=',$data['id'])->update($data);
		if($affected){
			return redirect('/camgroup')->with('success', 'Record Updated successfully!');
		}else{
			return redirect('/camgroup')->with('failed', 'Failed to Update!');
		}
    }
    public function delete($id){
    	$affected=DB::table('campaigngroup')->where('id','=',$id)->delete();
		if($affected){
			return redirect('/camgroup')->with('success', 'Record Deleted successfully!');
		}else{
			return redirect('/camgroup')->with('failed', 'Failed to Delete Record!');
		}

    }
}
