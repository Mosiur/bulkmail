<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use File;
use Image;

class templateController extends Controller{
	public function index(){
		$campaignid=Session::get('campaign')['campaignid'];
        $camgroup=DB::table('campaigngroup')->where('campaignid','=',$campaignid)->get();
		$data=DB::table('template')->where('campaignid','=',$campaignid)->get();
		return view('pages/template', ['templates' => $data,'camgroup'=>$camgroup]);
	}

	public function create(){
		$campaignid=Session::get('campaign')['campaignid'];
		$data=DB::table('campaign')->where('id','=',$campaignid)->get();
		$camgroup=DB::table('campaigngroup')->where('campaignid','=',$campaignid)->get();
		return view('pages/create_template', ['campaign' => $data,'campaigngroup'=>$camgroup]);
	}
	public function insert(Request $request){
		$campaignid=$request->input('campaignid');
		$campaigngroup=$request->input('campaigngroup');
		$subject=$request->input('subject');
		$message=$request->input('messagebody');
    	$file=$request->file('attachment');//File Name


    	$extention=$file->getClientOriginalExtension();//File Extention
    	$newimage_name=time().'.'.$extention;
    	$image_resize = Image::make($file->getRealPath());   
    	$image_resize->resize(255,291);
    	$image_resize->save('assets/uploads/'.$newimage_name);

    	$affected=DB::table('template')->insert(['campaignid'=>$campaignid,'campaigngroup'=>$campaigngroup,'subject'=>$subject,'message'=>$message,'attachment'=>$newimage_name]);
    	if($affected){
    		return redirect('/all_template')->with('success', 'Record Store successfully!');
    	}else{
    		return redirect('/all_template')->with('failed', 'Failed to Store Rcord..!');
    	}
    }

    public function edit(){
    	$campaignid=Session::get('campaign')['campaignid'];
    	$id=$_GET['id'];
    	$data=DB::table('template')->WHERE('id','=',$id)->first();
    	$campaign=DB::table('campaign')->where('id','=',$campaignid)->get();
    	$camgroup=DB::table('campaigngroup')->where('campaignid','=',$campaignid)->get();

    	?>
    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Campaign:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<select class="form-control select2-no-search" name="campaignid" required="required">
    				<?php
    				foreach ($campaign as $key => $camvalue) {?>
    					<option <?php if($camvalue->id == $data->campaignid){echo "selected";} ?> value="<?php echo $camvalue->id;?>">
    						<?php echo $camvalue->campaignname;?>
    					</option>
    					<?php
    				}
    				?>
    			</select>
    		</div>
    	</div>

    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Campaign Group:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<select class="form-control select2-no-search" name="campaigngroup" required="required">
    				<?php 
    				foreach($camgroup as $value){?>
    					<option <?php if($value->id == $data->campaigngroup){echo "selected";} ?> value="<?php echo $value->id;?>">
    						<?php echo $value->name;?>
    					</option>
    					<?php
    				}
    				?>
    			</select>
    		</div>
    	</div>

    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Subject:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<input name="subject" class="form-control" placeholder="Enter subject of mail" type="text" required="required" value="<?php echo $data->subject;?>">
    		</div>
    	</div>

    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Messsage:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<textarea required type="text" class="ckeditor" name="messagebody" placeholder="Enter replay message" ><?php echo $data->message;?></textarea>
    		</div>
    	</div>
    	<script>
    		CKEDITOR.replace( 'messagebody', {
    		} );
    	</script>


    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0">Attachment:</label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<input name="attachment" class="form-control" placeholder="Enter delay time for send message" type="file">
    		</div>
    	</div>
        <input type="hidden" name="id" value="<?php echo $id;?>">

    	<div class="row row-xs align-items-center mg-b-20">
    		<div class="col-md-3">
    			<label class="form-label mg-b-0"></label>
    		</div>
    		<div class="col-md-9 mg-t-5 mg-md-t-0">
    			<button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5" type="submit">Save Changes</button>
    		</div>
    	</div>
    	<?php
    }

    public function update(Request $request){

        $id=$request->input('id');
        $campaignid=$request->input('campaignid');
        $campaigngroup=$request->input('campaigngroup');
        $subject=$request->input('subject');
        $message=$request->input('messagebody');
        
        if ($request->hasFile('attachment')) {
            $attachment=DB::table('template')->select('attachment')->WHERE('id','=',$id)->first();
            unlink('assets/uploads/'.$attachment->attachment);
            $file=$request->file('attachment');//File Name
            $extention=$file->getClientOriginalExtension();//File Extention
            $newimage_name=time().'.'.$extention;
            $image_resize = Image::make($file->getRealPath());   
            $image_resize->resize(255,291);
            $image_resize->save('assets/uploads/'.$newimage_name);

            $affected=DB::table('template')->WHERE('id','=',$id)->update(['campaignid'=>$campaignid,'campaigngroup'=>$campaigngroup,'subject'=>$subject,'message'=>$message,'attachment'=>$newimage_name]);
            if($affected){
                return redirect('/all_template')->with('success', 'Record Updated successfully!');
            }else{
                return redirect('/all_template')->with('failed', 'Failed to Update Rcord..!');
            }
        }
        $affected=DB::table('template')->WHERE('id','=',$id)->update(['campaignid'=>$campaignid,'campaigngroup'=>$campaigngroup,'subject'=>$subject,'message'=>$message]);
        if($affected){
            return redirect('/all_template')->with('success', 'Record Updated successfully!');
        }else{
            return redirect('/all_template')->with('failed', 'Failed to Update Rcord..!');
        }

    }

    public function delete($id){
        $affected=DB::table('template')->WHERE('id','=',$id)->delete();
        if($affected){
            return redirect('/all_template')->with('success', 'Record Deleted successfully!');
        }else{
            return redirect('/all_template')->with('failed', 'Failed to Delete Rcord..!');
        }

    }
}
