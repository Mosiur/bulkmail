<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class campaignController extends Controller
{
    public function index(){
        $campaignid=Session::get('campaign')['campaignid'];
    	$campaigns = DB::table('campaign')->where('id','=',$campaignid)->get();
        return view('pages.managecampaign', ['campaigns' => $campaigns]);
    }
    public function edit(){
    	$id=$_GET['id'];
        $data=DB::table('campaign')->WHERE('id','=',$id)->get();
        foreach ($data as $key => $value) {
            ?>
            <!-- Row -->
            <div class="card-body">
                <div class="main-content-label mg-b-5">
                    Edit Campaign
                </div>
                <div class="pd-30 pd-sm-40 bg-light">
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Campaign Name</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="campaign_name" class="form-control" placeholder="Enter Campaign Name" type="text" value="<?php echo $value->campaignname;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Exp Date</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="campaigndescription" class="form-control" placeholder="Enter Campaign Description" type="date" value="<?php echo $value->campaigndescription;?>">
                        </div>
                    </div>
                    <div class="row row-xs align-items-center mg-b-20">
                        <div class="col-md-4">
                            <label class="form-label mg-b-0">Campaign limit</label>
                        </div>
                        <div class="col-md-8 mg-t-5 mg-md-t-0">
                            <input name="campaignlimit" class="form-control" placeholder="Enter your email Password" type="text" value="<?php echo $value->campaignlimit;?>">
                        </div>
                    </div>
                    <input type="hidden" name="id" class="form-control" type="text" value="<?php echo $value->id;?>">
                    <button type="submit" class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">Save Changes</button>
                    <button class="btn btn-dark pd-x-30 mg-t-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <!--/Row-->
            <?php
        }
    }
    public function update(Request $request){
    	$id = $request->input('id');
    	$campaign_name = $request->input('campaign_name');
    	$campaigndescription = $request->input('campaigndescription');
    	$campaignlimit = $request->input('campaignlimit');
    	

    	$affected=DB::table('campaign')->WHERE('id',$id)->update(['campaignname'=>$campaign_name,'campaigndescription'=>$campaigndescription,'campaignlimit'=>$campaignlimit]);
    	
    	if($affected){
    		return redirect('/managecampaign')->with('success', 'Record Updated successfully!');
    	}else{
    		return redirect('/managecampaign')->with('failed', 'Record Updated Failed!');
    	}
    	
    	
    }
}
