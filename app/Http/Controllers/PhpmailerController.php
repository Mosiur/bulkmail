<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
// for install PHPMailer run command =composer require phpmailer/phpmailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use DB;
use Session;

class PhpmailerController extends Controller{

	public function sendEmail () {
			require '../vendor/autoload.php';	// load Composer's autoloader

			//$mail = new PHPMailer(true);  // Passing `true` enables exceptions


			//Active mastermail
			$campaignid=Session::get('campaign')['campaignid'];
			$mastermail=DB::table('mastermail')->where([['maxlimit','>',0],['campaignid','=',$campaignid]])->first();
			if ($mastermail != NULL) {
				$hits=DB::table('corn')->select('hits')->where('campaignid','=',$campaignid)->first();
				$receiver=DB::table('maillist')->where([['campaignid','=',$campaignid],['campaingroup','=',$mastermail->group],['varifiedmail','=',1],['status','=',1],['flag','=',0]])->limit($hits->hits)
					->get();
				if(!$receiver->isEmpty()){
					foreach ($receiver as $key => $value) {
						$template=DB::table('template')->where([
							['campaignid','=',$value->campaignid],
							['campaigngroup','=',$value->campaingroup]
						])->first();

						$mail = "";
						$this->mail = new PHPMailer();
						$this->mail->SMTPOptions = array(
							'ssl' => array(
								'verify_peer' => false,
								'verify_peer_name' => false,
								'allow_self_signed' => true
							)
						);

						$this->mail->SMTPDebug = 0;
						$this->mail->isHTML(true);
						$this->mail->isSMTP();
						$this->mail->Host = $mastermail->host;
						$this->mail->Port = $mastermail->port;
						if($mastermail->protocol == 1){
							$this->mail->SMTPSecure = 'ssl';
						}
						if ($mastermail->protocol == 2) {
							$this->mail->SMTPSecure = 'tls';
						}

						$this->mail->SMTPAuth = true;
						$this->mail->Username = $mastermail->username;
						$this->mail->Password = $mastermail->password;
						$this->mail->From =  $mastermail->username;
						$this->mail->FromName = $mastermail->username;
						$this->mail->AddReplyTo($mastermail->username);
						//$this->mail->addCustomHeader('In-Reply-To', $message_id);


						$this->mail->addAddress($value->emailaddress);
						$this->mail->Subject = $template->subject;
						// $this->mail->AltBody  =  $template;
						// $this->mail->Body = $template;
						$this->mail->Body .= $template->message;

						$filepath = public_path()."/assets/uploads/" . $template->attachment;

						$this->mail->addAttachment($filepath, $template->attachment);

						if ($this->mail->Send()) {
							echo " Mail send successfully... ";
							DB::table('campaign')->where('id','=',$campaignid)->update(['campaignlimit'=>DB::raw('campaignlimit - 1')]);

							DB::table('mastermail')->where([['maxlimit','>',0],['campaignid','=',$campaignid]])->update(['maxlimit'=>DB::raw('maxlimit - 1')]);

							DB::table('maillist')->where([['campaignid','=',$campaignid],['campaingroup','=',$mastermail->group],['status','=',1],['flag','=',0]])->update(['flag'=>1]);
						}else{
							echo " mail Could not be send ";
						}
					}
				}else{
					echo "Mail List empty....";
				}
			}else{
				echo "Your master mail limit expire...";
			}
		}
		public function etrack($email,$id){
			if (!empty($_GET['email'])) {
				$id = $_GET['id'];
				$checkid = "Id:" . $id;
				$email = $_GET['email'];
				$sub = $_GET['subject'];
				date_default_timezone_set('Asia/Kolkata');
				$date = date('d/m/Y h:i:s a');
				DB::table('leads')->where([['fromemail','=',$email],['leadtime','=',$id]])->update(['eopen'=>'eopen'+1,'leadtime'=>'leadtime'+1]);
    		//All done, get out!
				exit;
			}	
		}
	} 
