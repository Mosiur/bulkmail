<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class dashboard extends Controller
{
    public function autoload(){
        $campaignid=Session::get('user')['campaignid'];
        $campaigns = DB::table('campaign')->where('id','=',$campaignid)->get();
        return view('layouts.autoload',['campaigns'=>$campaigns]);

    }
    public function index(){

    	$campaignid=Session::get('user')['campaignid'];
    	$campaignname=Session::get('campaign')['campaignname'];
    	$today=strtotime(date('Y-m-d'));
    	$tadaysend=DB::table('maillist')->where([['campaignid','=',$campaignid],['flag','=',1],['varifiedmail','=',1],['status','=',1],['updated_at','=',$today]])->count();
    	$notsend=DB::table('maillist')->where([['campaignid','=',$campaignid],['flag','=',0],['varifiedmail','=',1],['status','=',1]])->count();
    	$totalsend=DB::table('maillist')->where([['campaignid','=',$campaignid],['flag','=',1],['varifiedmail','=',1],['status','=',1]])->count();


    	$validmail=DB::table('maillist')->where([['campaignid','=',$campaignid],['varifiedmail','=',1]])->count();
    	$invalidmail=DB::table('maillist')->where([['campaignid','=',$campaignid],['varifiedmail','=',0]])->count();
    	$total=DB::table('maillist')->where([['campaignid','=',$campaignid]])->count();

    	$campaign=DB::table('campaign')->where([['id','=',$campaignid]])->get();
    	$mastermails = DB::table('mastermail')->select('username','group','maxlimit','flag')->where('campaignid','=',$campaignid)->get();
    	$campaigngroup=DB::table('campaigngroup')->where('campaignid','=',$campaignid)->get();

    	return view('pages.dashboard',['tadaysend'=>$tadaysend,'notsend'=>$notsend,'totalsend'=>$totalsend,'valid'=>$validmail,'invalid'=>$invalidmail,'totalmail'=>$total,'campaign'=>$campaign,'mastermails'=>$mastermails,'campaigngroup'=>$campaigngroup]);
    }
}
