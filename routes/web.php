<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


 //Clear route cache:
 Route::get('/route-cache', function() {
     $exitCode = Artisan::call('route:cache');
     return 'Routes cache cleared';
 });

 //Clear config cache:
 Route::get('/config-cache', function() {
     $exitCode = Artisan::call('config:cache');
     return 'Config cache cleared';
 }); 

// Clear application cache:
 Route::get('/clear-cache', function() {
     $exitCode = Artisan::call('cache:clear');
     return 'Application cache cleared';
 });

 // Clear view cache:
 Route::get('/view-clear', function() {
     $exitCode = Artisan::call('view:clear');
     return 'View cache cleared';
 });
 
Route::get('/admin', function () { return view('pages/login');});
Route::post('/signin','auth@index')->name('signin');
Route::group(['middleware'=>['userAuthentication']],function(){
	
	Route::get('/','dashboard@index');
	Route::get('/autoload','dashboard@autoload');
	// Route::get('/autoload', function () {return view('layouts.autoload');});


	// Campaign
	Route::get('/managecampaign','campaignController@index');
	Route::get('/edit_campaign','campaignController@edit')->name('edit_campaign');
	Route::post('/updt_campaign','campaignController@update');


	//Campaign Group
	Route::get('/camgroup', 'camgroupController@index');
	Route::get('/add_camgroup', 'camgroupController@add');
	Route::post('/storegroup','camgroupController@store');
	Route::get('/edit_camgroup', 'camgroupController@edit');
	Route::post('/updt_camgroup', 'camgroupController@update');
	Route::get('/delete_camgroup/{id}', 'camgroupController@delete')->name('delete_camgroup');


	Route::get('/addpopmail','popmailController@addpopmail');
	Route::post('/insertpopmail','popmailController@insertpopmail');
	Route::get('/popmail','popmailController@index');
	Route::get('/edit_mastermail','popmailController@edit_mastermail');
	Route::post('/updt_mastermail','popmailController@update');
	Route::get('/delete_mastermail/{id}','popmailController@delete')->name('delete_mastermail');

	//tempalte
	Route::get('/create_template','templateController@create');
	Route::get('/all_template','templateController@index');
	Route::post('/inserttemplate','templateController@insert');
	Route::get('/edit_template','templateController@edit');
	Route::post('/updt_template','templateController@update');
	Route::get('/delete_template/{id}','templateController@delete')->name('delete_template');
});


// Administration
Route::group(['middleware'=>['adminAccess']],function(){
	//User management
	Route::get('/manageuser','userController@alluser');
	Route::post('/adduser','userController@insert');
	Route::get('/edit_user','userController@edituser');
	Route::post('/updateuser','userController@updateuser');

	//Reset System
	Route::get('/reset_system','resetController@reset_system');
	Route::post('/reset','resetController@resetsystem');
	
	// mail Library
	Route::get('/mail_library','maillibraryController@index');
	Route::post('/updateact1','maillibraryController@updateact1');
	Route::post('/delete_smtp','maillibraryController@delete_smtp');
	Route::post('/uploadcsv','maillibraryController@uploadcsv');

	//Conversation
	Route::get('/sendmail','PhpmailerController@sendEmail');
	Route::get('/corn','cornController@corn');
	Route::get('/add_corn', 'cornController@add');
	Route::post('/storecorn','cornController@store');
	Route::get('/edit_corn', 'cornController@edit');
	Route::post('/updt_corn', 'cornController@update');

});
Route::get('/etrack/{email}/{id}','PhpmailerController@etrack')->name('etrack');
Route::get('/logout', function () {
	Session::flush();
	return redirect('/admin')->with('status', 'You are looged out...!');
});